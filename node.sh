#!/bin/bash

#install rmate 
apt-get update
curl https://raw.githubusercontent.com/aurora/rmate/master/rmate > /usr/local/bin/rsub 
chmod +x /usr/local/bin/rsub

#install npm, nodeJS, nodemon and pm2
curl -sL https://deb.nodesource.com/setup_4.x | bash -
apt-get install -y nodejs
npm install -g nodemon
npm install -g pm2 