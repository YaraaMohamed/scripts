#!/bin/bash

#install docker
wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker ($whoami)
sudo systemctl enable docker.service
sudo systemctl start docker.service

#install docker compose
sudo yum install -y python-pip
sudo pip install docker-compose
sudo yum upgrade python*
sudo pip install --upgrade docker-compose